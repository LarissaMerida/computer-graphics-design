#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>

void init(void)
{
      //Seta a janela (background) na cor Verde
      glClearColor(0.0,0.0,0.0,0.0);
      //Parâmetros de projeção BI-Dimensional
      glMatrixMode(GL_PROJECTION);
      gluOrtho2D(0.0,260.0,150.0,0.0);
}

void setpoint(int x,int y, float r, float g, float b, int line){
    glPointSize(line);

    glBegin(GL_POINTS);
        glColor3f(r, g, b);
        glVertex2i(x, y);
    glEnd();

    glFlush();
}


void ponto_circulo(int x, int y, int valor, int xc, int yc,int option, float r, float g, float b)
{
    if(option == 4){
      //Semi circulo superior
      setpoint(x+xc, y+yc, r, g, b, valor);
      setpoint(-x+xc, y+yc, r, g, b, valor);
    } else if (option == 5){
      //Semi circulo inferior
      setpoint(-x+xc, -y+yc, r, g, b, valor);
      setpoint(x+xc, -y+yc, r, g, b, valor);
    } else {
      //Circulo completo 
      if(option != 2){
          setpoint(x+xc, y+yc, r, g, b, valor);
          setpoint(y+xc, x+yc, r, g, b, valor);
          setpoint(y+xc, -x+yc, r, g, b, valor);
          setpoint(x+xc,-y +yc, r, g, b, valor);
      }
      //Circulo completo ou semi circulo superior
      if(option != 3){
          setpoint(-x+xc, -y+yc, r, g, b, valor);
          setpoint(-y+xc, -x+yc, r, g, b, valor);
          setpoint(-y+xc, x+yc, r, g, b, valor);
          setpoint(-x+xc, y+yc, r, g, b, valor);
      }
    }
} /*ponto_circulo*/

//Inspirado em https://marciobueno.com/arquivos/ensino/cg/CG_03_Primitivas_Graficas.pdf

void bresenhamCirculo(int raio, int valor,int xc,int yc,int option, float r, float g, float b){
    int x = 0 ;
    int y = raio;
    double d = 1.25 - raio;
    int c = 0;

    ponto_circulo(x , y , valor, xc, yc,option, r, g, b);
    while(y > x) {
        if (d < 0) /* escolhe E */
            d += 2.0*x + 3.0;
        else { /* escolhe SE */
            d += 2*(x-y) + 5;
            y--;
        }
        x++;
        ponto_circulo(x,y,valor,xc,yc,option, r, g, b);
    } /* while*/
} /*pontomedio*/


void bresenham(GLfloat x1, GLfloat x2, GLfloat y1, GLfloat y2, float r, float g, float b, int line){

 	glLineWidth(line);
    glColor3f(r, g, b);

    GLfloat dx, dy, incE, incNE, d;
    int x, y, i;
    dx = x2 - x1;
    dy = y2 - y1;
    d = 2*dy - dx;
    incE = 2*dy;
    incNE = 2*(dy-dx);
    x = x1;
    y = y1;

    setpoint(x, y, r, g, b, line);
    if(dx == 0){
        for(i = y1;i <= y2; i++)
        	setpoint(x, i, r, g, b, line);
    }else if(dy == 0){
        for(i = x1;i <= x2; i++)
        	setpoint(i, y, r, g, b, line);
    }else{
        while(x <= x2){
            if(d <= 0){
                d += incE;
                x = x + 1;
            }else{
                d += incNE;
                x = x + 1;
                y = y + 1;
            }
            setpoint(x, y, r, g, b, line);
        }
    }
}

void display(void)
{
    //Limpa a Tela e habilita os valores glClearColor
    glClear(GL_COLOR_BUFFER_BIT);

    //seta a cor da linha
    glColor3f(0.0,0.0,1.0);
    int thickness = 1;
    float r = 255, g = 255, b = 255;

    //desenha retangulo pricnipal
    bresenham(50,50,30, 130, r, g, b, thickness);
    bresenham(150, 150, 30, 130, r, g, b, thickness);
    bresenham(50, 150,30, 30, r, g, b,  thickness);
    bresenham(50, 150, 130, 130, r, g, b,  thickness);

    //desenha retangulos menores
    bresenham(50, 150, 40, 40, r, g, b,  thickness);
    bresenham(50, 150, 120, 120, r, g, b,  thickness);
    bresenham(50, 150, 66, 66, r, g, b,  thickness);
    bresenham(50, 150, 79, 79, r, g, b,  thickness);
    bresenham(50, 150, 92, 92, r, g, b,  thickness);

    //linhas que tocam circulos
    bresenham(62, 66, 51, 51, r, g, b,  thickness);
    bresenham(62, 66, 55, 55, r, g, b,  thickness);

    bresenham(84, 88, 51, 51, r, g, b,  thickness);
    bresenham(84, 88, 55, 55, r, g, b,  thickness);

    bresenham(107, 111, 51, 51, r, g, b,  thickness);
    bresenham(107, 111, 55, 55, r, g, b,  thickness);

    bresenham(129, 133, 51, 51, r, g, b,  thickness);
    bresenham(129, 133, 55, 55, r, g, b,  thickness);

    //linhas que tocam circulos

    bresenham(62, 66, 104, 104, r, g, b,  thickness);
    bresenham(62, 66, 108, 108, r, g, b,  thickness);

    bresenham(84, 88, 104, 104, r, g, b,  thickness);
    bresenham(84, 88, 108, 108, r, g, b,  thickness);

    bresenham(107, 111, 104, 104, r, g, b,  thickness);
    bresenham(107, 111, 108, 108, r, g, b,  thickness);

    bresenham(129, 133, 104, 104, r, g, b,  thickness);
    bresenham(129, 133, 108, 108, r, g, b,  thickness);


    //desenha circulo
    float xstep = 100.0f;
    float ystep = 79.0f;
    float raio = 6.0f;
    //2 circulos maiores de cima

    bresenhamCirculo(raio, thickness, xstep, 27.9, 4, r, g, b);
    bresenhamCirculo(raio, thickness, xstep, 133, 5, r, g, b);


    bresenhamCirculo(raio, thickness, xstep, ystep, 1, r, g, b);
    setpoint(xstep, ystep, r, g, b, 7);

    bresenhamCirculo(raio*1.5f, thickness, 75, 53, 1, r, g, b);
    //Pontos
    setpoint(75, 53, r, g, b, thickness);
    setpoint(75, 69, r, g, b, thickness);
    setpoint(75, 89, r, g, b, thickness);


    bresenhamCirculo(raio*1.5f, thickness, 75, 106, 1, r, g, b);
    //Pontos
    setpoint(75, 106, r, g, b, thickness);

    bresenhamCirculo(raio*1.5f, thickness, 120, 53, 1, r, g, b);
    //Pontos
    setpoint(120, 53, r, g, b, thickness);
    setpoint(120, 69, r, g, b, thickness);
    setpoint(120, 89, r, g, b, thickness);

    bresenhamCirculo(raio*1.5f, thickness, 120, 106, 1, r, g, b);
    //Pontos
    setpoint(120, 106, r, g, b, thickness);

    glFlush();
}

int main (int argc, char** argv) {

       //estabelece a contato com o sistema de janelas
       glutInit (&argc, argv);
       glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
       glutCreateWindow("Bresenham");

       glutInitWindowSize(1000,600);
       glutInitWindowPosition(10,10);

       init();
       glutDisplayFunc(display);
       //glCallList(0);
       glutMainLoop();

       //cursor position now in p.x and p.y
      //  while (true) {
      //   if (GetCursorPos(&p)) {
      //     printf("%s x", p.x);
      //     printf("%s\n y", p.y);
      //
      //   }
      // }

}
