#include <iostream>
#include <GL/glut.h>
#include <math.h>

#define PI 3.1415926535897932384626433832795

void init(void) {
      //Seta a janela (background)
      glClearColor(0.0,0.0,0.0,0.0);
      //Parâmetros de projeção BI-Dimensional
      glMatrixMode(GL_PROJECTION);
      gluOrtho2D(0.0,200.0,150.0,0.0);
}


void DrawRet(float r, float g, float b){
     // Desenha um quadrado preenchido com a cor corrente
    glBegin(GL_LINES);
      glColor3f(r, g, b);
      //linhas da extermidade
      glVertex2f(50,10);
      glVertex2f(120,10);

      glVertex2f(50,110);
      glVertex2f(120,110);

      glVertex2f(50,10);
      glVertex2f(50,110);

      glVertex2f(120,10);
      glVertex2f(120,110);
    glEnd();

    glBegin(GL_LINES);
      glColor3f(r, g, b);
      //linhas interiores
      glVertex2f(50,20);
      glVertex2f(120,20);

      glVertex2f(50,100);
      glVertex2f(120,100);

      glVertex2f(50, 60);
      glVertex2f(120,60);

      glVertex2f(50,40);
      glVertex2f(120,40);

      glVertex2f(50, 80);
      glVertex2f(120, 80);

    glEnd();
}


<<<<<<< HEAD
void createcircle (float xstep,float ystep, float raio, float r, float g, float b, int thickness) {
=======
void createcircle (int xstep,int ystep, float raio, float r, float g, float b, int thickness) {
>>>>>>> cd06baecee4d0f620bf9f62ea65272cfffdf47d1

    //Inspirado em http://professor.unisinos.br/ltonietto/jed/cgr/IntroOpenGL.pdf
    //Circulo do meio da quadra
    int circle_points = 10000000;
    double angle = 0;
    int i;

    glBegin(GL_LINE_LOOP);
    glLineWidth(thickness);
    glColor3f(r, g, b);
    for (i = 0; i < circle_points; i++) {
        angle = 2*PI*i/circle_points;
        glVertex2i((cos(angle)*raio) + xstep, (sin(angle)*raio) + ystep);
    }
    glEnd();
}

void display(void) {
    //Limpa a Tela e habilita os valores glClearColor
    glClear(GL_COLOR_BUFFER_BIT);
    int thickness = 2;

    //seta a cor da linha
    float r = 255, g  = 255, b = 255;
    DrawRet(r, g, b);
    //desenha ciruclo maior
    createcircle(  85, 60, 11, r, g, b, thickness);

    //circulos menores de cima
    createcircle(  65, 30, 5, r, g, b, thickness);
    createcircle(  100, 30, 5, r, g, b, thickness);

    //circulos menores de baixo
    createcircle(  65, 90, 5, r, g, b, thickness);
    createcircle(  100, 90, 5, r, g, b, thickness);

    glFlush();
}

int main (int argc, char** argv) {

       //estabelece a contato com o sistema de janelas
       glutInit (&argc, argv);
       glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
        glutCreateWindow("Drawing");

       glutInitWindowSize(1000,600);
       glutInitWindowPosition(10,10);

       init();
       glutDisplayFunc(display);
       //glCallList(0);

       glutMainLoop();
}
