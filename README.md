# computer graphics design

Comparison between methodss<br /><br />


Projeto de Computação Gráfica Tarefa 1<br />
Valor 2 Pontos Instruções:<br />
Data: 06/02<br />
Grupo: 2 Alunos<br />
Objetivo: Avaliar a diferença de precisão usando o alg. do Ponto Médio proposto por Bresenham.<br /><br />

  a) Desenhar uma Quadra de Ice Hockey (https://bit.ly/2BTmI7h) usando as equações da reta e de circunferência;  <br />
  b) Desenhar uma Quadra de Ice Hockey usando Bresenham: retas e circunferências; <br /> 
  c) A arquibancada será desenhada pelo usuário que irá clicar em pontos da tela e a partir destes pontos será traçada retas que definirão o objeto (arquibancada);<br />
  d) O usuário irá selecionar entre os algoritmos de Bresenham ou eq. da reta para realizar o traçado;<br />
  e) Através da interface, o usuário definirá a cor das retas e a espessura;<br />
  f) É obrigatório o uso do Opengl e um framework de janelas (QT,FLTK,JAVA-SWING, etc) para realizar o projeto.<br /><br />

Bom Trabalho!<br />
prof. Marcelo
<br /><br />
Na pasta HockeySquare/dist,  execute com : java -jar HockeySquare.jar