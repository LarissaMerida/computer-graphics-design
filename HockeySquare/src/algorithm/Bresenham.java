/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package algorithm;
import com.jogamp.opengl.GL2;

/**
 *
 * @author valdix
 */
public class Bresenham {
    
    void setpoint(float x,float y, GL2 gl){
        
        gl.glBegin(GL2.GL_POINTS);
        gl.glVertex2d(x, y);
        gl.glEnd();
        
    }
    
    public void drawPointCircle(float x, float y, float xc, float yc, int option, GL2 gl) {
        
        if(option == 4){
            //Semi circulo superior
            this.setpoint(x+xc, y+yc,gl);
            this.setpoint(-x+xc, y+yc ,gl);
            
        } else if (option == 5){
            //Semi circulo inferior
            this.setpoint(-x+xc, -y+yc,gl);
            this.setpoint(x+xc, -y+yc,gl);
        } else {
            //Circulo completo ou semi circulo direito
            if(option != 2){
                this.setpoint(x+xc, y+yc,gl);
                this.setpoint(y+xc, x+yc,gl);
                this.setpoint(y+xc, -x+yc,gl);
                this.setpoint(x+xc, -y+yc,gl);
            }
            //Circulo completo ou semi circulo esquerdo
            if(option != 3){
                this.setpoint(-x+xc, -y+yc,gl);
                this.setpoint(-y+xc, -x+yc,gl);
                this.setpoint(-y+xc, x+yc,gl);
                this.setpoint(-x+xc, y+yc,gl);
            }
        }
    }
    //public void Circulo(int raio,int xc,int yc,int option, float r, float g, float b) {
    public void circulo(float raio, float xc, float yc, int option, GL2 gl) {
        
        float x = 0 ;
        float y = raio;
        double d = 1.25 - raio;
        int c = 0;
        
        this.drawPointCircle(x,y,xc,yc,option,gl);
        while(y > x) {
            if (d < 0) /* escolhe E */
                d += 2.0*x + 3.0;
            else { /* escolhe SE */
                d += 2*(x-y) + 5;
                y--;
            }
            x++;
            drawPointCircle(x,y,xc,yc,option,gl);
        } // while*/
    }
    
    
    void bresenham(float x1, float x2, float y1, float y2, GL2 gl) {
        
        float dx, dy, incE, incNE, d;
        float x, y, i;
        dx = x2 - x1;
        dy = y2 - y1;
        d = 2*dy - dx;
        incE = 2*dy;
        incNE = 2*(dy-dx);
        x = x1;
        y = y1;
        
        this.setpoint(x, y,gl);
        if(dx == 0){
            for(i = y1;i <= y2; i++)
                this.setpoint(x, i,gl);
        }else if(dy == 0){
            for(i = x1;i <= x2; i++)
                this.setpoint(i, y,gl);
        }else{
            while(x <= x2){
                if(d <= 0){
                    d += incE;
                    x = x + 1;
                }else{
                    d += incNE;
                    x = x + 1;
                    y = y + 1;
                }
                this.setpoint(x, y,gl);
            }
        }
    }
    
    public float zoom(float t, float z) {
        return t*z;
    }
    
    void DrawLine(GL2 gl){
        //desenha linhas menores
        // circulo 1
        this.bresenham(290, 310, 590, 590, gl);
        this.bresenham(290, 310, 610, 610, gl);
        this.bresenham(390, 410, 590, 590, gl);
        this.bresenham(390, 410, 610, 610, gl);
        
        //circulo 2
        this.bresenham(590, 610, 590, 590, gl);
        this.bresenham(590, 610, 610, 610, gl);
        this.bresenham(690, 710, 590, 590, gl);
        this.bresenham(690, 710, 610, 610, gl);
        
        //circulo 3
        this.bresenham(590, 610, 290, 290, gl);
        this.bresenham(590, 610, 310, 310, gl);
        this.bresenham(690, 710, 290, 290, gl);
        this.bresenham(690, 710, 310, 310, gl);
        
        //circulo 4
        this.bresenham(290, 310, 290, 290, gl);
        this.bresenham(290, 310, 310, 310, gl);
        this.bresenham(390, 410, 290, 290, gl);
        this.bresenham(390, 410, 310, 310, gl);    
    }

    public void display(GL2 gl) {
        
        //desenha retangulo pricnipal
        this.bresenham(200, 800, 700, 700, gl);
        this.bresenham(200, 800, 200, 200, gl);
        this.bresenham(200, 200, 200, 700, gl);
        this.bresenham(800, 800, 200, 700, gl);
        
        //desenha retangulos menores
        this.bresenham(200, 800, 650, 650, gl);
        this.bresenham(200, 800, 450, 450, gl);
        this.bresenham(200, 800, 250, 250, gl);
        this.bresenham(200, 800, 535, 535, gl);
        this.bresenham(200, 800, 365, 365, gl);
        
        DrawLine(gl);
        
        //desenha circulo
        float xstep = 500.0f;
        float ystep = 450.0f;
        float raio = 50f;
        
        //Circulo central
        this.circulo(70, 500, 450, 1, gl);
        this.setpoint(xstep, ystep,gl);
        
        //semicirculo
        this.circulo(raio, 500, 167, 4, gl);
        this.circulo(raio, 500, 734, 5, gl);
        //ciruclos inferiores
        this.circulo(raio-10, 350, 300, 1, gl);
        this.circulo(raio-10, 650, 300, 1, gl);
        //ciruclos superiores
        this.circulo(raio-10, 350, 600, 1, gl);
        this.circulo(raio-10, 650, 600, 1, gl);
        
        //desenha pontos 
        //esquerda
        this.circulo(1, 350, 300, 1, gl);
        this.circulo(1, 350, 380, 1, gl);
        this.circulo(1, 350, 525, 1, gl);
        this.circulo(1, 350, 600, 1, gl);
        //direita
        this.circulo(1, 650, 300, 1, gl);
        this.circulo(1, 650, 380, 1, gl);
        this.circulo(1, 650, 525, 1, gl);
        this.circulo(1, 650, 600, 1, gl);
        //ponto central
        this.circulo(3, 500, 450, 1, gl);
    }
}
