package algorithm;

/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/


import com.jogamp.opengl.GL2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

/**
 *
 * @author larissa
 */
public class Naive {
    
    void createcircle (float xstep,float ystep, float raio, GL2 gl, float thickness, int option) {
        
        //Inspirado em http://professor.unisinos.br/ltonietto/jed/cgr/IntroOpenGL.pdf
        //Circulo do meio da quadra
        float circle_points = 1000;
        float angle = 0;
        int i, op_q = 1, op_angleX = 1,op_angleY = 1 ;
        double PI = 3.1415926535897932384626433832795, xx, yy, xxx, yyy;
        gl.glLineWidth(thickness);
        gl.glBegin(GL2.GL_LINE_LOOP);
        //se for semicirculo multiplica por 2 
        
        //semicirculo superior
        if(option == 2){
            op_q = 2;
            op_angleX = 1;
            op_angleY = 1;
        }
        //semicirculo inferior
        else if(option == 3){
            op_q = 2;
            op_angleX = -1;
            op_angleY = -1;
        }
        // 2 quadrante x +- e y+
        else if(option == 4){
            op_q = 4;
            op_angleX = -1;
            op_angleY = 1;
        }
        // 3 quadrante x e y  -
        else if(option == 5){
            op_q = 4;
            op_angleX = -1;
            op_angleY = -1;
        }
        // 4 quadrante x e y -
        else if(option == 6){
            op_q = 4;
            op_angleX = -1;
            op_angleY = -1;
        }
        
        for (i = 0; i < circle_points; i++) {
            angle = (float) (2*PI*i/op_q/circle_points);
            gl.glVertex2d( op_angleX*(cos(angle)*raio)+xstep, op_angleY*(sin(angle)*raio)+ystep);   
        }
        gl.glEnd();
    }
    
    void DrawLine(GL2 gl, float thickness){
        //pontos
        gl.glBegin(GL2.GL_LINES);
            //linhas interiores
            //Pontos do primeiro circulos
            //esquerda
            gl.glVertex2f(310,590);
            gl.glVertex2f(290,590);
            
            gl.glVertex2f(310,610);
            gl.glVertex2f(290,610);
            //direita
            gl.glVertex2f(410,590);
            gl.glVertex2f(390,590);
            
            gl.glVertex2f(410,610);
            gl.glVertex2f(390,610);
            
            //Pontos do secundo circulos
            //esquerda
            gl.glVertex2f(610,590);
            gl.glVertex2f(590,590);
            
            gl.glVertex2f(610,610);
            gl.glVertex2f(590,610);
            //direita
            gl.glVertex2f(710,590);
            gl.glVertex2f(690,590);
            
            gl.glVertex2f(710,610);
            gl.glVertex2f(690,610);
            
            //Pontos do terceiro circulo
            //esquerda
            gl.glVertex2f(610,290);
            gl.glVertex2f(590,290);
            
            gl.glVertex2f(610,310);
            gl.glVertex2f(590,310);
            //direita
            gl.glVertex2f(710,290);
            gl.glVertex2f(690,290);
            
            gl.glVertex2f(710,310);
            gl.glVertex2f(690,310);
            
            //Pontos do quarto circulo
            //esquerda
            gl.glVertex2f(310,290);
            gl.glVertex2f(290,290);
            
            gl.glVertex2f(310,310);
            gl.glVertex2f(290,310);
            //direita
            gl.glVertex2f(410,290);
            gl.glVertex2f(390,290);
            
            gl.glVertex2f(410,310);
            gl.glVertex2f(390,310);
            
        gl.glEnd();
    }
    
    void DrawRet(GL2 gl, float thickness){
        // Desenha um quadrado 
        gl.glBegin(GL2.GL_LINES);
            gl.glLineWidth(thickness);
        
            //linhas da extermidade
            gl.glVertex2f(200,700);
            gl.glVertex2f(800,700);
        
            gl.glVertex2f(200,200);
            gl.glVertex2f(800,200);
        
            gl.glVertex2f(200,700);
            gl.glVertex2f(200,200);
        
            gl.glVertex2f(800,700);
            gl.glVertex2f(800,200);
        gl.glEnd();
             
        gl.glBegin(GL2.GL_LINES);
            //linhas interiores
            //Primeira
            gl.glVertex2f(200,650);
            gl.glVertex2f(800,650);
            //ultima
            gl.glVertex2f(200, 250);
            gl.glVertex2f(800,250);
            
            //linha da metade
            gl.glVertex2f(800,450);
            gl.glVertex2f(200,450);
            
            //linha metade superior
            gl.glVertex2f(800,535);
            gl.glVertex2f(200,535);
            //linha metade inferior
            gl.glVertex2f(800,365);
            gl.glVertex2f(200,365);
        
        gl.glEnd();     
    }
    
    public Naive(GL2 gl, float thickness) {
        //Limpa a Tela e habilita os valores glClearColor
     
        this.DrawRet(gl, thickness);
        this.DrawLine(gl, thickness);
        //desenha circulo maior
        this.createcircle(  500, 450, 70, gl, thickness, 1);
        
        //circulos menores de baixo
        this.createcircle(  350, 300, 40, gl, thickness, 1);
        this.createcircle(  650, 300, 40, gl, thickness, 1);
        
        //circulos menores de cima
        this.createcircle(  350, 600, 40, gl, thickness, 1);
        this.createcircle(  650, 600, 40,gl, thickness, 1);
        
        // semicirculos 
        this.createcircle(  500, 700, 25,gl, thickness, 3);
        this.createcircle(  500, 200, 25,gl, thickness, 2);
        
        //pontos esquerda
        this.createcircle(  350, 600, 1,gl, thickness, 1);
        this.createcircle(  350, 525, 1,gl, thickness, 1);
        this.createcircle(  350, 380, 1,gl, thickness, 1);
        this.createcircle(  350, 300, 1,gl, thickness, 1);
        //pontos direita
        this.createcircle(  650, 600, 1,gl, thickness, 1);
        this.createcircle(  650, 525, 1,gl, thickness, 1);
        this.createcircle(  650, 380, 1,gl, thickness, 1);
        this.createcircle(  650, 300, 1,gl, thickness, 1);
        //ponto central
        this.createcircle(  500, 450, 3,gl, thickness, 1);
        
        //this.createcircle(  150, 200, 50,gl, thickness, 5);
        gl.glFlush();
    }
}
