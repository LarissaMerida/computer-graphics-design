package menu;

import com.jogamp.opengl.GL2;

abstract class Painting {
    abstract void drawLine(int x1, int x2, int y1, int y2, GL2 gl);
    abstract void drawCircle(int raio, int xc,int yc,int option, GL2 gl);
}