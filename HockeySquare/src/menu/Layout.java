package menu;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import algorithm.Bresenham;
import algorithm.Naive;

import static com.jogamp.opengl.GL.GL_COLOR_BUFFER_BIT;
import static com.jogamp.opengl.GL.GL_DEPTH_BUFFER_BIT;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.FPSAnimator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
/**
 *
 * @author larissa
 */
public class Layout extends GLCanvas implements GLEventListener, MouseListener, MouseMotionListener, ActionListener {
    
    private static String TITLE = "Ice Hockey Court";
    private static final int WIDTH = 1300;
    private static final int HEIGHT = 780;
    private static final int FPS = 60;
    private static int firstClickX = -1;
    private static int firstClickY = -1;
    private static final int TICK_MIN = 0;
    private static final int TICK_INIT = 2;
    private static final int TICK_MAX = 10;
    
    //espessura de linha inicial
    private static float thickness = 1;
    private static int algorithmSelected = 0;
    private static double red = 2, green = 3, blue = 4;
    
    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                GLCanvas canvas = new Layout();
                canvas.setPreferredSize(new Dimension(WIDTH, HEIGHT));
                
                final FPSAnimator animator = new FPSAnimator(canvas, FPS, true);
                final JFrame frame = new JFrame();
                
                frame.addWindowListener(new WindowAdapter() {
                    @Override   
                    public void windowClosing(WindowEvent e) {
                        new Thread(() -> {
                            if (animator.isStarted()) animator.stop();
                            System.exit(0);
                        }).start();
                    }
                });
                //create janela
                JPanel p = new JPanel();
                p.setLayout(new FlowLayout());
                frame.setTitle(TITLE);
                JPanel botoes = new JPanel();
                
                JRadioButton naiveButton = new JRadioButton("Naive");
                JRadioButton bresButton = new JRadioButton("Bresenham");
                
                //naive selecionado?
                naiveButton.addActionListener(e -> {
                    if (naiveButton.isSelected()) {
                        algorithmSelected = 0;
                    }
                });
                
                //bresenham selecionado?
                bresButton.addActionListener(e -> {
                    if (bresButton.isSelected()) {
                        algorithmSelected = 1;
                    }
                });
                
                naiveButton.setSelected(true);
                
                //pega espessura
                JSlider espessura = new JSlider(JSlider.HORIZONTAL, TICK_MIN, TICK_MAX, TICK_INIT);
                
                espessura.addChangeListener(e -> thickness = espessura.getValue());
                
                JLabel lOpcao = new JLabel("Algoritmo");
                JLabel lEspessura = new JLabel("Espessura");
                
                ButtonGroup buttonGroup = new ButtonGroup();
                buttonGroup.add(naiveButton);
                buttonGroup.add(bresButton);
                
                //exibe paleta de cores
                JButton botaoCor = new JButton("Cor");
                botaoCor.setOpaque(true);
                 botaoCor.addActionListener(e -> {
                    Color color = JColorChooser.showDialog(null, "Choose the color for the lines", Color.BLUE);
                    System.out.println(color);
                    
                    if (color != null) {
                        botaoCor.setBackground(color);
                        red = color.getRed() / 255;
                        green = color.getGreen() / 255;
                        blue = color.getBlue() / 255;
                    }
                });
                
                JButton clearLines = new JButton("Clean");
                clearLines.setOpaque(true);
                clearLines.addActionListener(e -> {
                        thickness = 1;
                        red = 2;
                        green = 3;
                        blue = 4;
                        algorithmSelected = 0;    
                    
                });
                            
                botoes.setLayout(new BoxLayout(botoes, BoxLayout.Y_AXIS));
                botoes.add(botaoCor);
                botoes.add(clearLines);
                botoes.add(lOpcao);
                botoes.add(naiveButton);
                botoes.add(bresButton);
                botoes.add(lEspessura);
                botoes.add(espessura);
                
                Container pane = frame.getContentPane();
                pane.add(botoes, BorderLayout.LINE_START);
                pane.add(canvas);
                frame.pack();
                frame.setVisible(true);
                animator.start();
            }
        });
    }

    public Layout() {
        this.addGLEventListener(this);
        addMouseListener(this);
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        GLU glu = new GLU();
        glu.gluOrtho2D(0,WIDTH, 0, HEIGHT);
    }
    
    private void draw(GL2 gl) {
        
    if (algorithmSelected == 1) {
      Bresenham bresenham = new Bresenham();
      bresenham.display(gl);
    } else {
        Naive naive = new Naive(gl, thickness);   }
    }
    
    public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        gl.glClearColor(0.78f, 0.64f, 0.78f, 1);
        gl.glColor3d(red, green, blue);
        gl.glPointSize(thickness);
        draw(gl);
    }

    public void mousePressed(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        y = Math.abs(y - HEIGHT);
        
        System.out.println("x: " + x + " y: " + y);
        
        if (firstClickX == -1 && firstClickY == -1) {
            firstClickX = x;
            firstClickY = y;
        } else {
            firstClickX = -1;
            firstClickY = -1;
        }
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {}

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {}

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    @Override
    public void actionPerformed(ActionEvent e) {}

    @Override
    public void mouseDragged(MouseEvent e) {}

    @Override
    public void mouseMoved(MouseEvent e) {} 
}
